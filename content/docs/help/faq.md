---
title: "FAQ"
description: "Réponses aux questions les plus fréquentes."
lead: "Réponses aux questions les plus fréquentes."
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "aide"
weight: 630
toc: true
---

## Changer son mot de passe de boite fonctionnelle?

- [Oups mon mdp](https://portail.ac-rennes.fr/oupsmdpfonc/OupsMdpFonc)

## Autre documentation?

- [Hugo](https://gohugo.io/documentation/)

## Le support?

- [Amigo](https://assistance.ac-rennes.fr)
