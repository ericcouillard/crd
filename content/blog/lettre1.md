---
title: "Lettre d'information n°1"
description: ""
lead: "Le sites écoles/EPLE ont été rénovés."
date: 2021-04-16T14:01:25+02:00
lastmod: 2021-04-16T14:01:25+02:00
draft: false
weight: 50
images: []
contributors: [Eric Couillard]
---



### Charte Marianne

Le logo académique a changé

![logoa acad](/Acad_marianne_170px.jpg "clic droit enregistrer sous...")

### SPIP 3.2.9

Le CMS est monté en version stable 3.2.9

> Oh la belle version !
