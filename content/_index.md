---
title : "Centre de ressources et documentation"
description: "Le centre de ressources et documentation de la DSI du rectorat de Rennes"
lead: "Trouvez ici les informations nécessaires à votre quotidien numérique"
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---
![](/Acad_marianne_170px.jpg)
